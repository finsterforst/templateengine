# finsterforst\template-engine

All commands are based right from the root directory, if not otherwise stated. 

## Install

As project
```
git clone https://finsterforst@bitbucket.org/finsterforst/templateengine.git
cd templateengine
composer update
```

As library
```
composer config repositories.finsterforst/template-engine vcs https://finsterforst@bitbucket.org/finsterforst/templateengine.git
composer require finsterforst/template-engine
```


## Testing
```
cd tests
../vendor/bin/phpunit
```