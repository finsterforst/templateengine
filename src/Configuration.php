<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine;


use Finsterforst\TemplateEngine\Exception\PathNotFoundException;

class Configuration
{
    /** @var string[] */
    private $paths = [];

    /**
     * Provide an absolute path to the target template directory
     *
     * @param string $path
     * @throws PathNotFoundException
     */
    public function addPath(string $path) : void
    {
        // File exists also checks if a directory exists
        if (!file_exists($path)) {
            throw new PathNotFoundException();
        }
        $this->paths[] = $path;
    }

    /**
     * @return string[]
     */
    public function getPaths() : array
    {
        return $this->paths;
    }
}