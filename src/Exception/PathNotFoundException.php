<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine\Exception;


class PathNotFoundException extends \Exception {}