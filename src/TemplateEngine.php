<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine;


use Finsterforst\TemplateEngine\Exception\CouldNotLoadTemplate;
use Finsterforst\TemplateEngine\Exception\TemplateEngineWasNotInitialised;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class TemplateEngine
{
    private static $instance;

    /** @var FilesystemLoader */
    private $filesystemLoader;

    /** @var Environment */
    private $twig = null;

    /** @var string */
    private $rendered = '';

    private function __construct() {}
    private function __clone() {}

    public static function getInstance() : TemplateEngine
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param Configuration $config
     */
    public function initialise(Configuration $config) : void
    {
        $this->initialiseFilesystemLoader($config);
        $this->initialiseEnvironment();
    }

    /**
     * @param Configuration $config
     */
    private function initialiseFilesystemLoader(Configuration $config) : void
    {
        $this->filesystemLoader = new FilesystemLoader($config->getPaths());
    }

    private function initialiseEnvironment() : void
    {
        $this->twig = new Environment($this->filesystemLoader, [
            'Cache' => __DIR__ . '/cache/'
        ]);
    }

    public function getTwig(){return $this->twig;}

    /**
     * @param string $templateName
     * @throws CouldNotLoadTemplate
     * @throws TemplateEngineWasNotInitialised
     */
    public function render(string $templateName) : void
    {
        if (!$this->filesystemLoader instanceof FilesystemLoader) {
            throw new TemplateEngineWasNotInitialised('The object FilesystemLoader is missing');
        }

        if (!$this->twig instanceof Environment) {
            throw new TemplateEngineWasNotInitialised('The object Environment (twig) is missing');
        }

        try {
            $this->rendered = $this->twig->render(
                $templateName . '.twig',
                TemplateVariableCollection::getInstance()->getCollection()
            );
        } catch (LoaderError | RuntimeError | SyntaxError $exception) {
            throw new CouldNotLoadTemplate($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    public function display() : void
    {
        echo $this->rendered;
    }
}
