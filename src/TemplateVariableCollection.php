<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine;


class TemplateVariableCollection
{
    private static $instance;

    private $collection = [];

    private function __construct() {}
    private function __clone() {}

    public static function getInstance() : self
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add(string $key, $value) : void
    {
        $this->collection[$key] = $value;
    }

    /**
     * @return string[]
     */
    public function getCollection() : array
    {
        return $this->collection;
    }
}