<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine\Test;


use PHPUnit\Framework\TestCase;

class BaseTestClass extends TestCase
{
    /** @var stdClass[] */
    private $testFiles = [];

    public function tearDown(): void
    {
        foreach ($this->testFiles as $file) {
            if (file_exists($file->path)) {
                unlink($file->path);
            }
        }

        parent::tearDown();
    }


    public function helperRegisterTestFile(string $filename = 'testfile')
    {
        $object = new \stdClass();
        $object->name = $filename;
        $object->path = '../src/cache/cache_'.$filename.'.json';

        $this->testFiles[] = $object;

        return $object;
    }

    public function helperResetSingleton($singelton)
    {
        $reflection = new \ReflectionClass($singelton);
        $instance = $reflection->getProperty('instance');
        $instance->setAccessible(true); // now we can modify that :)
        $instance->setValue(null, null); // instance is gone
        $instance->setAccessible(false); // clean up
    }
}