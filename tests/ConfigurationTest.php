<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine\Test;


use Finsterforst\TemplateEngine\Configuration;
use Finsterforst\TemplateEngine\Exception\PathNotFoundException;

class ConfigurationTest extends BaseTestClass
{
    public function testAddPaths()
    {
        $testPath_0 = 'ConfigurationTest/Views/';
        $testPath_1 = 'ConfigurationTest/Views/Home/';
        $configuration = new Configuration();
        $configuration->addPath($testPath_0);
        $configuration->addPath($testPath_1);

        $this->assertEquals($configuration->getPaths()[0], $testPath_0);
        $this->assertEquals($configuration->getPaths()[1], $testPath_1);
    }

    public function testAddPathsInvalidPath()
    {
        $this->expectException(PathNotFoundException::class);

        $configuration = new Configuration();
        $configuration->addPath('foobar/');
    }
}