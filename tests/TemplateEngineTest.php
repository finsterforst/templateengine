<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine\Test;


use Finsterforst\TemplateEngine\Configuration;
use Finsterforst\TemplateEngine\Exception\CouldNotLoadTemplate;
use Finsterforst\TemplateEngine\Exception\TemplateEngineWasNotInitialised;
use Finsterforst\TemplateEngine\TemplateEngine;

class TemplateEngineTest extends BaseTestClass
{
    public function testRenderExpectsObjectFilesystemLoader()
    {
        $this->helperResetSingleton(TemplateEngine::getInstance());

        $this->expectException(TemplateEngineWasNotInitialised::class);

        $templateEngine = TemplateEngine::getInstance();
        $templateEngine->render('foobar');
    }
}