<?php declare(strict_types=1);


namespace Finsterforst\TemplateEngine\Test;


use Finsterforst\TemplateEngine\TemplateVariableCollection;

class TemplateVariableCollectionTest extends BaseTestClass
{
    public function testAdd()
    {
        $this->helperResetSingleton(TemplateVariableCollection::getInstance());

        $instance = TemplateVariableCollection::getInstance();
        $instance->add('a', 'b');

        $this->assertEquals($instance->getCollection()['a'], 'b');
    }
}